<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title> Hello There!</title>

  <!-- Bootstrap Core CSS -->
  <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom Fonts -->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">
  <link href="vendor/simple-line-icons/css/simple-line-icons.css" rel="stylesheet">

  <!-- Custom CSS -->
  <link href="css/stylish-portfolio.min.css" rel="stylesheet">

</head>

<body id="page-top">

  <!-- Navigation -->
  <a class="menu-toggle rounded" href="#">
    <i class="fas fa-bars"></i>
  </a>
  <nav id="sidebar-wrapper">
    <ul class="sidebar-nav">
      <li class="sidebar-brand">
        <a class="js-scroll-trigger" href="#page-top"> Silahkan Pilih Menu </a>
      </li>
      <li class="sidebar-nav-item">
        <a class="js-scroll-trigger" class="nav-link" href="index.html">Home</a>
      </li>
      <li class="sidebar-nav-item">
        <a class="js-scroll-trigger" class="nav-link" href="insertdata.php"> Login </a>
      </li>
      <li class="sidebar-nav-item">
        <a class="js-scroll-trigger" class="nav-link" href="lihatdata3.php"> Pelayanan </a>
      </li>
       <li class="sidebar-nav-item">
        <a class="js-scroll-trigger" class="nav-link" href="insertdata2.php"> Transaksi </a>
      </li>
      <li class="sidebar-nav-item">
        <a class="js-scroll-trigger" class="nav-link" href="lihatdata.php"> Data Pelanggan </a>
      </li>
      <li class="sidebar-nav-item">
        <a class="js-scroll-trigger" class="nav-link" href="lihatdata1.php"> Data Pegawai</a>
      </li>
      <li class="sidebar-nav-item">
        <a class="js-scroll-trigger" class="nav-link" href="lihatdata2.php"> Data Transaksi</a>
      </li>
    </ul>
  </nav>

  <!-- Portfolio -->
 <?php
  include "koneksi.php";


  $data = mysqli_query($koneksi,"SELECT * FROM pegawai");
  $no = 1;
?>
<div class="container" style="margin-top:20px">
    <h4>Data Pegawai</h4>
    <hr>
<div class="table-responsive   table-sm ">
  <table class="table">

  <tr>
    <th>No</th>
    <th>ID Pegawai</th>
    <th>Nama</th>
    <th>AKSI</th>
  </tr>

<?php

foreach ($data as $x) { ?>

  <tr>
    <td><?= $no?></td>  
    <td><?= $x['id_pegawai']?></td>
    <td><?= $x['nama_pegawai']?></td>
    <td>
      <a href="edit1.php?id_pegawai=<?php echo $x['id_pegawai'];?>"><i class="fa fa-edit" title="Edit"></i></a>&nbsp&nbsp&nbsp&nbsp
      <a href="delete1.php?id_pegawai=<?php echo $x['id_pegawai'];?>"><i class="fa fa-trash" title="Hapus"></i>
  </tr>

<div>

<?php
$no++; } 
?>
</div>

  </table>

      <div class="form-group row">
        <label class="col-sm-2 col-form-label">&nbsp;</label>
        <div class="col-sm-10">
          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
          &nbsp;&nbsp;&nbsp;&nbsp;
          <a href="insertdata1.php" class="btn btn-warning"> Tambah Data </a>
        </div>
      </div>

  <!-- Footer -->
  <footer class="footer text-center">
    <div class="container">
      <ul class="list-inline mb-5">
        <li class="list-inline-item">
          <a class="social-link rounded-circle text-white mr-3" href="#">
            <i class="icon-social-facebook"></i>
          </a>
        </li>
        <li class="list-inline-item">
          <a class="social-link rounded-circle text-white mr-3" href="#">
            <i class="icon-social-twitter"></i>
          </a>
        </li>
        <li class="list-inline-item">
          <a class="social-link rounded-circle text-white" href="#">
            <i class="icon-social-github"></i>
          </a>
        </li>
      </ul>
      <p class="text-muted small mb-0">Copyright &copy; Andikha 2019</p>
    </div>
  </footer>

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded js-scroll-trigger" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Bootstrap core JavaScript -->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Plugin JavaScript -->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for this template -->
  <script src="js/stylish-portfolio.min.js"></script>

</body>

</html>
