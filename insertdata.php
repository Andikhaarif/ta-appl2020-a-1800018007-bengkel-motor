<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title> Hello There!</title>

  <!-- Bootstrap Core CSS -->
  <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom Fonts -->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">
  <link href="vendor/simple-line-icons/css/simple-line-icons.css" rel="stylesheet">

  <!-- Custom CSS -->
  <link href="css/stylish-portfolio.min.css" rel="stylesheet">

</head>

<body id="page-top">

  <!-- Navigation -->
  <a class="menu-toggle rounded" href="#">
    <i class="fas fa-bars"></i>
  </a>
  <nav id="sidebar-wrapper">
    <ul class="sidebar-nav">
      <li class="sidebar-brand">
        <a class="js-scroll-trigger" href="#page-top"> Silahkan Pilih Menu </a>
      </li>
      <li class="sidebar-nav-item">
        <a class="js-scroll-trigger" class="nav-link" href="index.html">Home</a>
      </li>
      <li class="sidebar-nav-item">
        <a class="js-scroll-trigger" class="nav-link" href="insertdata.php"> Login </a>
      </li>
      <li class="sidebar-nav-item">
        <a class="js-scroll-trigger" class="nav-link" href="lihatdata3.php"> Pelayanan </a>
      </li>
       <li class="sidebar-nav-item">
        <a class="js-scroll-trigger" class="nav-link" href="insertdata2.php"> Transaksi </a>
      </li>
      <li class="sidebar-nav-item">
        <a class="js-scroll-trigger" class="nav-link" href="lihatdata.php"> Data Pelanggan </a>
      </li>
      <li class="sidebar-nav-item">
        <a class="js-scroll-trigger" class="nav-link" href="lihatdata1.php"> Data Pegawai</a>
      </li>
      <li class="sidebar-nav-item">
        <a class="js-scroll-trigger" class="nav-link" href="lihatdata2.php"> Data Transaksi</a>
      </li>
    </ul>
  </nav>

  <!-- Portfolio -->
  <div class="container" style="margin-top:20px">
    <h2> Login </h2>
    <hr>
    <div class="jumbotron">
       <div class="container">
    <form method="post">
      <div class="form-group row">
        <label class="col-sm-2 col-form-label">ID Pelanggan</label>
        <div class="col-sm-10">
          <input type="text" name="id_pelanggan" class="form-control" size="4" required>
        </div>
      </div>
      <div class="form-group row">
        <label class="col-sm-2 col-form-label">Nama</label>
        <div class="col-sm-10">
          <input type="text" name="nama_pelanggan" class="form-control" required>
        </div>
      </div>
      <div class="form-group row">
        <label class="col-sm-2 col-form-label">Alamat</label>
        <div class="col-sm-10">
          <textarea name="alamat" class="form-control"></textarea>
        </div>
      </div>
      <div class="form-group row">
        <label class="col-sm-2 col-form-label">No Hp</label>
        <div class="col-sm-10">
          <input type="text" name="no_hp" class="form-control" required>
        </div>
      </div>
      
      
        
        
      <div class="form-group row">
        <label class="col-sm-2 col-form-label">&nbsp;</label>
        <div class="col-sm-10">
          <input type="submit" name="submit" class="btn btn-primary" value="Masuk">
        </div>
      </div>
    </form>
    </div>
  </div>
  </div>


<?php
include "koneksi.php";
  if(isset($_POST['submit'])){
  $id_pelanggan  = $_POST['id_pelanggan'];
      $nama_pelanggan  = $_POST['nama_pelanggan'];
      $alamat = $_POST['alamat'];
      $no_hp = $_POST['no_hp'];
      

    $cek = mysqli_query($koneksi, "SELECT * FROM pelanggan WHERE id_pelanggan='$id_pelanggan'") or die(mysqli_error($koneksi));
      
    if(mysqli_num_rows($cek) == 0){
      $sql = mysqli_query($koneksi, "INSERT INTO pelanggan (id_pelanggan, nama_pelanggan, alamat, no_hp) VALUES('$id_pelanggan', '$nama_pelanggan', '$alamat', '$no_hp' )") or die(mysqli_error($koneksi));
      
      if($sql){
          echo '<script>alert("Berhasil menambah data."); document.location="tambah.php";</script>';
        }else{
          echo '<div class="alert alert-warning">Gagal memproses penambahan data.</div>';
        }
      }else{
        echo '<div class="alert alert-warning">Gagal, ID sudah terdaftar.</div>';
      }
    }
    ?>

  <!-- Footer -->
  <footer class="footer text-center">
    <div class="container">
      <ul class="list-inline mb-5">
        <li class="list-inline-item">
          <a class="social-link rounded-circle text-white mr-3" href="#">
            <i class="icon-social-facebook"></i>
          </a>
        </li>
        <li class="list-inline-item">
          <a class="social-link rounded-circle text-white mr-3" href="#">
            <i class="icon-social-twitter"></i>
          </a>
        </li>
        <li class="list-inline-item">
          <a class="social-link rounded-circle text-white" href="#">
            <i class="icon-social-github"></i>
          </a>
        </li>
      </ul>
      <p class="text-muted small mb-0">Copyright &copy; Andikha 2019</p>
    </div>
  </footer>

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded js-scroll-trigger" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Bootstrap core JavaScript -->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Plugin JavaScript -->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for this template -->
  <script src="js/stylish-portfolio.min.js"></script>

</body>

</html>
